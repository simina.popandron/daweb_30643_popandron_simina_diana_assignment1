import React from "react";
import {Button, Container, Jumbotron} from "reactstrap";

import BackgroundImg from '../commons/images/cover.png';

const backgroundStyle = {
    backgroundPosition: 'center',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    width: "100%",
    height: "700px",
    backgroundImage: `url(${BackgroundImg})`,
    padding:"0%"
};

class Acasa extends React.Component {
    render(){
        return(
                    <Jumbotron fluid style={backgroundStyle} className="container-fluid page_background_color full text-acasa">
                        <Container fluid>
                            <p className="text_acasa ">O aplicatie nascuta datorita nivelului crescut de poluare cauzat de numarul crescut de masini care se afla zinlnic in trafic in Cluj-Napoca.</p>
                            <p className="text_acasa ">Putem salva orasul cu fiecare pas facut, intr-un mod distractiv si folositor pentru noi.</p>
                        </Container>
                    </Jumbotron>
        );
    }
}

export default Acasa;