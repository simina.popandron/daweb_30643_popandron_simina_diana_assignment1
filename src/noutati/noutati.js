import React from "react";
import clock from '../commons/images/Coming-Soon-Icon.png';

class Noutati extends React.Component {
    render(){
        return(
       <div className="container-fluid page_background_color full">
           <div className="holder">
               <img src={clock} />
               <h1><span className="tbl">To Be Announced</span></h1>
               <p><span className="tbl">Revenim cu informatii in scurt timp</span></p><br/>
               <br/>
           </div>
       </div>
        );
    }
}

export default Noutati;