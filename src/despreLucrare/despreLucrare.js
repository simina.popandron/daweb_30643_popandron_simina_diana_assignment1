import React from "react";
import logo from '../commons/images/profile.png';
import cluj from '../commons/images/cluj.jpg';
import bus from '../commons/images/bus.jpg';
import walk from '../commons/images/walk.jpg';
import video from '../commons/images/video.mp4';

import ReactDOM from 'react-dom';
import "react-responsive-carousel/lib/styles/carousel.min.css";
import { Carousel } from 'react-responsive-carousel';

class DespreLucrare extends React.Component {
    render(){
        return(

            <div className="container-fluid page_background_color full">

                <div className="container text-center">
                    <div className="row" >
                        <div className="col-sm-5 col-md-6" >

                            <Carousel showThumbs={false} dynamicHeight>
                                <div>
                                    <img src={logo} />
                                </div>
                                <div>
                                    <img src={cluj} />
                                </div>
                                <div>
                                    <img src={bus} />
                                </div>
                                <div>
                                    <img src={walk} />
                                </div>
                                <div>
                                    <video width="500" height="500" controls >
                                        <source src={video} type="video/mp4"/>
                                    </video>
                                </div>
                            </Carousel>
                        </div>

                        <div className="col-sm-5 col-sm-offset-2 col-md-6 col-md-offset-0">
                            <h4 className={"text_mare_about"}>
                                Aplicatie Android pentru monitorizarea regimului de deplasare contorizarea tipului de deplasare
                            </h4>
                            <br/>
                            <h9 className={"text_mic_about"}>
                                Aplicatia va monitoriza modul de deplasare a utilizatorului(mers pe jos/transport in comun/masina), iar in functie de mijlocul folosit se vor acorda puncte. Similar oricarui joc, numarul de puncte rezulta in nivele, iar la fiecare nivel utilizarotul va fi recompensat.
                            </h9>
                        </div>
                    </div>
                    <br/>
                    <br/>

                    <h3 className={"text_mare_about"}>
                        Obiective
                    </h3>
                    <div className="row text_mic_about">
                        <div className="col-sm-3">
                                <p>➸ Indemnarea clujenilor de a folosi transportul in comun, in detrimentul masinilor personale sau a serviciilor de taxi/ car share-ing disponibile.</p>
                        </div>
                        <div className="col-sm-3">

                                <p>➸ Motivarea utilizatorilor de a merge pe jos cat mai mult, minimizand utilizarea oricarui mijloc de transport prin acordarea unor puncte care rezulta in premii fizice.</p>
                        </div>
                        <div className="col-sm-3">
                                <p>➸ Purificarea aerului datorita scaderii numarului de masini din trafic.</p>
                        </div>
                        <div className="col-sm-3">
                            <p>➸ Realizarea unei aplicatii folositoare atat pentru utilizator cat si pentru cei din jurul sau.</p>
                        </div>
                    </div>
                </div>




            </div>



        );
    }
}
export default DespreLucrare;