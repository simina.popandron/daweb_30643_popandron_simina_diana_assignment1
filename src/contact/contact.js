import React from "react";

class Contact extends React.Component {
    render(){
        return(

                <div className="container-fluid page_background_color full" >
                    <h1>Contact</h1>
                    <div className="row">
                        <div className="col-sm-5">
                            <h3>Simina Diana Popandron</h3>
                            <h7>Studenta UTCN, Facultatea de Automatica si Calculatoare (sectia TI) </h7>
                            <br/><br/>
                            <p>Pentru orice intrebare va rog sa trimiteti un mail la adresa de mai jos si va voi raspunde in maxim 24 de ore.</p>
                            <br></br>
                            <p>Numar de telefon: <a href={"https://www.whatsapp.com/"}> 0740180511 </a> </p>
                            <p>E-mail: <a href="https://www.google.com/intl/ro/gmail/about/"> simina.popandron@yahoo.com </a></p>
                            <p >Linked-in: <a href="https://www.linkedin.com/in/simina-popandron-849a45157/"> Simina Popandron </a> </p>
                        </div>
                        <div className="col-sm-5">
                            <div className="row">
                                <div className="col-sm-5 form-group">
                                    <input className="form-control secondary_background_color" id="nume" name="nume" placeholder="Nume" type="text"
                                           required></input>
                                </div>
                                <div className="col-sm-6 form-group">
                                    <input className="form-control " id="email" name="email" placeholder="Email"
                                           type="email" required></input>
                                </div>
                            </div>
                            <textarea className="form-control" id="text" name="text" placeholder="Text"
                                      rows="7"></textarea><br></br>
                            <div className="row">
                                <div className="col-sm-12 form-group">
                                    <button className="btn btn-default btn-outline-default waves-effect" type="submit">Trimite</button>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>

        );
    }
}

export default Contact;