import React from 'react'
import profile from './commons/images/profile.png';

import NavItem from "react-bootstrap/NavItem";

import {
    DropdownItem,
    DropdownMenu,
    DropdownToggle,
    Nav,
    Navbar,
    NavbarBrand,
    NavLink,
    UncontrolledDropdown
} from 'reactstrap';


const textStyle = {
    color: 'white',
    textDecoration: 'none'
};


const NavigationBar = () => (

    <div>
        <Navbar className="color_navbar navbar-inverse navbar-fixed-top nav-tabs " light expand="md" >
            <Nav tabs >
                <NavItem href="/" className="text_navbar">
                    <img src={profile} className="logo"/>
                </NavItem>
                <NavItem >
                    <NavLink href="/" className="text_navbar" >
                        Acasa
                    </NavLink>
                </NavItem>
                <NavItem>
                    <NavLink href="/noutati" className="text_navbar">
                        Noutati
                    </NavLink>
                </NavItem>
                <NavItem>
                    <NavLink href="/despreLucrare" className="text_navbar">
                        Despre lucrare
                    </NavLink>
                </NavItem>
                <NavItem>
                    <NavLink href="/profilStudent" className="text_navbar">
                        Profil student
                    </NavLink>
                </NavItem>
                <NavItem>
                    <NavLink href="/coordonator" className="text_navbar">
                        Coordonator
                    </NavLink>
                </NavItem>
                <NavItem>
                    <NavLink href="/contact" className="text_navbar">
                        Contact
                    </NavLink>
                </NavItem>
            </Nav>
        </Navbar>





    </div>


);

export default NavigationBar
