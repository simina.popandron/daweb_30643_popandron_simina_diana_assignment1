import React from 'react'
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom'
import NavigationBar from './navigation-bar'

import Acasa from "./acasa/acasa";
import Noutati from "./noutati/noutati";
import DespreLucrare from "./despreLucrare/despreLucrare";
import ProfilStudent from "./profilStudent/profilStudent";
import Coordonator from "./coordonator/coordonator";
import Contact from "./contact/contact";


class App extends React.Component {


    render() {

        return (
            <div >
            <Router>
                <div>
                    <NavigationBar />
                    <Switch>

                        <Route
                            exact
                            path='/'
                            render={() => <Acasa/>}
                        />

                        <Route
                            exact
                            path='/noutati'
                            render={() => <Noutati/>}
                        />

                        <Route
                            exact
                            path='/despreLucrare'
                            render={() => <DespreLucrare/>}
                        />

                        <Route
                            exact
                            path='/profilStudent'
                            render={() => <ProfilStudent/>}
                        />

                        <Route
                            exact
                            path='/coordonator'
                            render={() => <Coordonator/>}
                        />

                        <Route
                            exact
                            path='/contact'
                            render={() => <Contact/>}
                        />

                        {/*Error*/}

                    </Switch>
                </div>
            </Router>
            </div>
        )
    };
}

export default App
