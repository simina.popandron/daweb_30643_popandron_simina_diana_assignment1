import React from "react";
import iancu from '../commons/images/iancu.png';

class Coordonator extends React.Component {
    render(){
        return(
            <div className="container-fluid page_background_color full text-center container portfolio">
                <div className="row">
                    <div className="col-md-12">
                        <div className="heading">
                            <img src="https://image.ibb.co/cbCMvA/logo.png"/>
                        </div>
                    </div>
                </div>
                <div className="bio-info" >
                    <div className="row">
                        <div className="col-md-6" >
                            <div className="row">
                                <div className="col-md-12">
                                    <div className="bio-image">
                                        <img src={iancu} alt="iancu" width="250" height="300"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-md-6">
                            <div className="bio-content">
                                <h3>Coordonator</h3>
                                <h5>Bogdan Iancu</h5>
                                <h8>PhD, Senior Lecturer UTCN</h8>
                                <br/><br/>
                                <h6>Materii predate </h6>

                                <div className="list-type1">
                                    <ol>
                                        <li><a href="#">Retele de Calculatoare(curs&laborator)</a></li>
                                        <li><a href="#">Circuite Analogice si Numerice (laborator)</a></li>
                                        <li><a href="#">Proiect Protocoale si Retele de Comunicatii(laborator)</a></li>
                                        <li><a href="#">Tehnologii wireless si dispozitive mobile (laborator)</a></li>
                                    </ol>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

            </div>
        );
    }
}

export default Coordonator;